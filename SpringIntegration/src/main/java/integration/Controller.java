package integration;

import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;


@RestController
public class Controller {
	@Autowired
	private GreetingGateway gateway;

	@Autowired
	private OrderGateway orderGateway;

	@Autowired
	private RestOperations restTemplate;

	@RequestMapping("/greeting/{name}")
	public String getGreeting(@PathVariable("name") String name) {
		Message<String> helloMessage =  MessageBuilder.withPayload(name.toUpperCase()).build();

		String result = gateway.handleRequest(helloMessage);
		return result;
	}


	@PostMapping(path= "/order", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> createOrder(@RequestBody Order order) {
//		Message<Order> orderMessage =  MessageBuilder.withPayload(order).build();
//		orderGateway.handleRequest(orderMessage);

		String urlOrder = "http://localhost:8090";

		// verify order
		boolean verified = restTemplate.postForObject(urlOrder + "/verifyOrder", order, Boolean.class);
		if(!verified){
			System.out.println("Order verification failed");
			return new ResponseEntity<String>("Order verification failed", null, HttpStatus.BAD_REQUEST);
		}
		System.out.println("Order verification succeeded");

		// check order
		boolean checkOrder = restTemplate.postForObject(urlOrder + "/checkStock", order, Boolean.class);
		if(!checkOrder){
			System.out.println("Check stock failed");
			return new ResponseEntity<String>("Check stock  failed", null, HttpStatus.BAD_REQUEST);
		}
		System.out.println("Check stock  succeeded");

		// process payment
		boolean paymentSucceed = restTemplate.postForObject(urlOrder + "/processPayment", order, Boolean.class);
		if(!paymentSucceed){
			System.out.println("Payment failed");
			return new ResponseEntity<String>("Payment failed", null, HttpStatus.BAD_REQUEST);
		}
		System.out.println("Payment succeeded");

		// ship
		boolean shipmentSucceed = restTemplate.postForObject(urlOrder + "/shipOrder", order, Boolean.class);
		if(!shipmentSucceed){
			System.out.println("Shipment failed");
			return new ResponseEntity<String>("Shipment failed", null, HttpStatus.BAD_REQUEST);
		}
		System.out.println("Shipment succeeded");

		return new ResponseEntity<String>("Order successful", HttpStatus.OK);

	}



	@PostMapping("/verifyOrder")
	public boolean verifyOrder(@RequestBody Order order){
		return true;
	}

	@PostMapping("/checkStock")
	public boolean checkStock(@RequestBody Order order){
		return true;
	}

	@PostMapping("/processPayment")
	public boolean processPayment(@RequestBody Order order){
		return true;
	}

	@PostMapping("/shipOrder")
	public boolean shipOrder(@RequestBody Order order){
		return true;
	}




}
