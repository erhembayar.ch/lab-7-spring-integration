package integration;

public class ShippingService {

    public Order shipOrder(Order order) throws Exception {
         System.out.println("Shipping order " + order.getOrderNumber());
         return order;
    }

    public Order handle(Order order) {
        System.out.println("ShippingService receiving order: "+ order.getOrderNumber());
        return order;
    }
}
