package integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.messaging.Message;

import java.math.BigDecimal;

@SpringBootApplication
@ImportResource("classpath:integration.xml")
public class SpringIntegrationApplication implements CommandLineRunner {

	@Autowired
	private RestOperations restTemplate;

	@Autowired
	private OrderGateway orderGateway;

	@Autowired
	private OrderShippingGateway orderShippingGateway;

	@Autowired
	private RushOrderShippingGateway rushOrderShippingGateway;

	static ConfigurableApplicationContext context;
	static MessageChannel inputChannel;

	public static void main(String[] args) {
		SpringApplication.run(SpringIntegrationApplication.class, args);
//		inputChannel = context.getBean("orderreceivechannel",
//				MessageChannel.class);
//		context.close();
	}

	@Bean
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return restTemplate;
	}

	@Override
	public void run(String... args) throws Exception {

		// EXERCISE 1 Part 1
		Message<Order> orderMsg = MessageBuilder.withPayload(new Order(1L, 1000.00)).build();
		orderGateway.handleRequest(orderMsg);

		// EXERCISE 1 Part 2
		Order order = new Order(3,100);
		Order rushOrder = new RushOrder(5,200);

		Message<Order> orderMessage = MessageBuilder.withPayload(order).setHeader("orderType", "normal").build();
		Message<Order> rushOrderMessage =
				MessageBuilder.withPayload(rushOrder).setHeader("orderType", "rush").build();

		orderShippingGateway.handleRequest(orderMessage);
		orderShippingGateway.handleRequest(rushOrderMessage);


		// EXERCISE 2
		final String urlOrder = "http://localhost:8090";
		Order newOrder = new Order(11L, 1000.00);
		ResponseEntity<String> response = restTemplate.postForEntity(urlOrder + "/order", newOrder, String.class);

		System.out.println("Order response: " + response.toString());

		System.exit(1);
	}
}
