package integration;

public class WarehouseService {

    public Order receiveOrder(Order order) throws Exception {
        System.out.println("Warehouse order " + order.getOrderNumber());

        return order;
    }

    public Order handle(Order order) {
        System.out.println("WarehouseService receiving order: "+ order.getOrderNumber());
        return order;
    }
}
