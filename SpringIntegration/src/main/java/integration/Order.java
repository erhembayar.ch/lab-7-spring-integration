package integration;

public class Order {
    long orderNumber;
    double amount;

    public Order(long orderNumber, double amount) {
        this.orderNumber = orderNumber;
        this.amount = amount;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String toString(){
        return "order: nr="+orderNumber+" amount="+amount;
    }
}
