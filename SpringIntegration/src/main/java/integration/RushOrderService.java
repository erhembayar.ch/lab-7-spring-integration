package integration;

import org.springframework.integration.annotation.ServiceActivator;

public class RushOrderService {

//    @ServiceActivator(inputChannel = "rushorderservicechannel")
    public Order handle(Order order) {
        System.out.println("RushOrderService receiving order: "+ order.toString());
        return order;
    }
}
