package integration;

import org.springframework.integration.annotation.ServiceActivator;

public class OrderService {

//    @ServiceActivator(inputChannel = "orderservicechannel")
    public Order handle(Order order) {
        System.out.println("NormalOrderShippingService receiving order: "+ order.toString());
        return order;
    }
}
